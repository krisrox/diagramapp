package com.company;

import com.l2fprod.common.swing.JTipOfTheDay;
import com.l2fprod.common.swing.plaf.basic.BasicTipOfTheDayUI;
import com.l2fprod.common.swing.tips.DefaultTip;
import com.l2fprod.common.swing.tips.DefaultTipModel;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTree;


/**
 * Program <code>Zadanie projektowe</code>
 * Klasa <code>TODD</code> definiujaca okno Tips Of The Day
 * @author Michal Staruch
 * @version 1.0	5/05/2020
 */
public class TOTD extends JPanel {
	/**
	 * Konstruktor parametryczny klasy TOTD
	 */
	public TOTD(App TOTD) {
		try {
			showTOTD();
		}
		catch(Exception e) {
			MyLogger.writeLog("ERROR","Blad podczas tworzenia Tips Of The Day");
			System.out.println("ERROR - Blad podczas tworzenia Tips Of The Day");
		}
	}

	public void showTOTD(){
		DefaultTipModel tips = new DefaultTipModel();

		// plain text
		tips.add(new DefaultTip(
						"tip1",
						"Wskazowka numer 1"));

		// html text
		tips.add(new DefaultTip("tip2",
				"<html>Wskazowka numer 2 w formacie <b>HTML</b><br><center>"
						+ "<table border=\"1\">" + "<tr><td>1</td><td>Wskazowka 2.1</td></tr>"
						+ "<tr><td>2</td><td>Wskazowka 2.2</td></tr>"
						+ "<tr><td>3</td><td>Wskazowka 2.3</td></tr>" + "</table>"));

		// a Component
		tips.add(new DefaultTip("tip3", new JTree()));

		// an Icon
		tips.add(new DefaultTip("tip 4", new ImageIcon(BasicTipOfTheDayUI.class.getResource("TipOfTheDay24.gif"))));

		JTipOfTheDay totd = new JTipOfTheDay(tips);
		totd.setCurrentTip(0);

		totd.showDialog(new JFrame("title"));
	}
}