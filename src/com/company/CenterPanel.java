package com.company;

import org.freixas.jcalendar.JCalendarCombo;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;



/**
* Program <code>Zadanie projektowe</code>
* Klasa <code>CenterPanel</code> definiujaca centralny panel aplikacji
* @author Michal Staruch
* @version 1.0	10/04/2020
*/
public class CenterPanel extends JPanel implements ActionListener{

	private JPanel northPanel, southPanel;
	private JTextArea resultTextArea;
	private JButton submitButton;
	// deklaracja zmiennej typu JCalendarCombo o nazwie jccData
	private JCalendarCombo jccData;
	private TitledBorder titledBorder;
	private Border blackLine;
	/**
	 * Konstruktor bezparametrowy klasy CenterPanel
	 */
	public CenterPanel() {
		try {
			createGUI();
		}
		catch(Exception e) {
			MyLogger.writeLog("ERROR","Blad podczas tworzenia GUI CenterPanel");
			System.out.println("ERROR - Blad podczas tworzenia GUI CenterPanel");
		}

	}
	/**
	 * Metoda tworzacaca graficzny interfejs uzyytkownika
	 */
	public void createGUI() {
		this.setLayout(new GridLayout(2,2,5,5));

		// Utworzenie panelu z paramtrami i wynikiem
		northPanel = createNorthPanel();
		southPanel = createSouthPanel();

		// Utworzenie obiektow TextField
		this.add(northPanel);
		this.add(southPanel);
	}
	/**
	 * Metoda tworzaca panel
	 * @return zwraca obiekt JPanel
	 */
	public JPanel createNorthPanel() {
		JPanel jp = new JPanel();
		blackLine = BorderFactory.createLineBorder(Color.gray);
		titledBorder = BorderFactory.createTitledBorder(blackLine,
				"Parmetry wejsciowe");
		titledBorder.setTitleJustification(TitledBorder.CENTER);
		jp.setBorder(titledBorder);
		jp.setLayout(new FlowLayout());

		submitButton = new JButton("Submit");
		submitButton.addActionListener(this);

		// utworzenie instacji obiektu JCalendar
		jccData = new JCalendarCombo(
				Calendar.getInstance(),
				Locale.getDefault(),
				JCalendarCombo.DISPLAY_DATE,
				false
		);
		// ustawienie formatu daty
		jccData.setDateFormat(new SimpleDateFormat("yyyy-MM-dd"));

		jp.add(jccData);
		jp.add(submitButton);
		return jp;
	}
	/**
	 * Metoda tworzaca panel z wynikami
	 * * @return zwraca obiekt JPanel
	 */
	public JPanel createSouthPanel() {
		JPanel jp = new JPanel();
		blackLine = BorderFactory.createLineBorder(Color.gray);
		titledBorder = BorderFactory.createTitledBorder(blackLine, "Wynik");
		titledBorder.setTitleJustification(TitledBorder.CENTER);
		jp.setBorder(titledBorder);
		jp.setLayout(new BorderLayout());

		resultTextArea = new JTextArea();
		// zawijanie wierszy
		resultTextArea.setLineWrap(true);
		this.resultTextArea.append(" -->\n");
		jp.add(new JScrollPane(resultTextArea), BorderLayout.CENTER);
		return jp;
	}
	/**
	 * Metoda obslugujaca zdarzenie akcji
	 */
	@Override
	public void actionPerformed(ActionEvent ae) {
		if(ae.getSource() == submitButton) {
			// Pobranie daty do obiektu typu String
			// Miesiace liczone sa od 0 wiec trzeba dodac 1
			Calendar cal = jccData.getCalendar();
			String data = ""+cal.get(Calendar.YEAR)+"-";
			int miesiac = cal.get(Calendar.MONTH)+1;
			if(miesiac <= 9) data = data+"0"+String.valueOf(miesiac)+"-";
			else data = data+String.valueOf(miesiac)+"-";
			int dzien = cal.get(Calendar.DAY_OF_MONTH);
			if(dzien <= 9) data = data+"0"+String.valueOf(dzien);
			else data = data+String.valueOf(dzien);

			// zapisanie danych w polu TextArea
			resultTextArea.append("Data: "+data+"\n");
			InfoBottomPanel.setInfoString("Dodano date!");
		}
	}
	/**
	 * Metoda okreslajaca wartosci odstepow od krawedzi panelu
	 * (top,left,bottom,right)
	 */
	public Insets getInsets() {
		return new Insets(5,10,10,10);
	}
}
