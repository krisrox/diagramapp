package com.company;

import java.awt.*;
import java.awt.event.*;
import java.io.FileOutputStream;
import java.io.IOException;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
/**
 * Program <code>Zadanie projektowe</code>
 * Klasa <code>MainWindow</code> definiujaca glowne okno aplikacji
 * @author Michal Staruch
 * @version 1.0	10/04/2020
 */

public class MainWindow extends JDialog implements ActionListener {

	private JPanel northPanel, tablePanel, resultPanel, inPanel, buttonPanel;
	private JTextField digitTextField;
	private JTextArea resultTextArea;
	private Calc calculation;
	private JTable table;
	private JLabel digitLabel, rowLabel, colLabel;
	private JButton addButton, clearButton, saveButton, executeButton;
	private TitledBorder titledBorder;
	private JSpinner rowSpinner, colSpinner;
	private SpinnerNumberModel colValue, rowValue;
	private JList<String> list;
	private final String fileName = "result.txt";
	private Border blackLine;
	private final String[] labelButton = new String[]{"Oblicz", "Dodaj", "Zeruj", "Zapisz"};
	private final String[] labelList = new String[]{"Suma", "Srednia", "Max", "Min"};
	FileOutputStream file = null;
	
	/**
	 * Konstruktor bezparametrowy klasy <code>HelpWindow</code>
	 */
	public MainWindow() {
		this.setTitle("Strona glowna");
		this.setModal(false);
		this.setResizable(true);
		this.setSize(500,620);
		this.setLocationRelativeTo(null); //alternatywnie ustawia lokalizacje zawsze na srodku ekranu

		try {
			createGUI();
		}
		catch(Exception e) {
			MyLogger.writeLog("ERROR","Blad podczas tworzenia MainWindow");
			System.out.println("ERROR - Blad podczas tworzenia MainWindow");
		}
	}
	/**
	 * Metoda tworzacaca graficzny interfejs uzyytkownika
	 */
	public void createGUI() {
		this.setLayout(new GridLayout(3, 2, 5, 5));

		// Utworzenie panelu z paramtrami i wynikiem
		this.northPanel = this.createNorthPanel();
		this.tablePanel = this.createTablePanel();
		this.resultPanel = this.createResultPanel();

		// Utworzenie obiektow TextField
		add(northPanel);
		add(tablePanel);
		add(resultPanel);
	}
	/**
	 * Metoda tworzaca panel z parametrami oraz przyciskami
	 * @return zwraca obiekt JPanel
	 */
	public JPanel createNorthPanel() {
		JPanel createNP = new JPanel();
		blackLine = BorderFactory.createLineBorder(Color.gray);
		titledBorder = BorderFactory.createTitledBorder(blackLine,
				"Wprowadz dane");
		titledBorder.setTitleJustification(TitledBorder.CENTER);
		createNP.setBorder(titledBorder);
		createNP.setLayout(new FlowLayout());

		this.buttonPanel = this.createButtonPanel();
		this.inPanel = this.createInPanel();
		createNP.add(this.inPanel, "North");
		createNP.add(this.buttonPanel, "South");
		return createNP;
	}
	/**
	 * Metoda tworzaca panel z przyciskami
	 * @return zwraca obiekt JPanel
	 */
	public JPanel createButtonPanel(){
		JPanel createBP = new JPanel();
		executeButton = new JButton(this.labelButton[0]);
		executeButton.addActionListener(this);
		addButton = new JButton(this.labelButton[1]);
		addButton.addActionListener(this);
		clearButton = new JButton(this.labelButton[2]);
		clearButton.addActionListener(this);
		saveButton = new JButton(this.labelButton[3]);
		saveButton.addActionListener(this);
		createBP.add(executeButton);
		createBP.add(addButton);
		createBP.add(clearButton);
		createBP.add(saveButton);
		return createBP;
	}
	/**
	 * Metoda tworzaca liste z operacjami na liczbach
	 * @return zwraca obiekt JPanel
	 */
	public JPanel createInPanel(){
		JPanel createInP = new JPanel();

		this.colValue = new SpinnerNumberModel(1,1,5,1);
		this.rowValue = new SpinnerNumberModel(1,1,5,1);
		colLabel = new JLabel("Kolumna: ");
		rowLabel = new JLabel("Wiersz: ");
		this.colSpinner = new JSpinner(colValue);
		this.rowSpinner = new JSpinner(rowValue);

		this.colSpinner.setPreferredSize(new Dimension(50, 20));
		this.rowSpinner.setPreferredSize(new Dimension(50, 20));

		this.rowSpinner.addChangeListener(ce -> {
		});

		this.colSpinner.addChangeListener(ce -> {
		});

		digitLabel = new JLabel("Wprowadz liczbe: ");
		digitTextField = new JTextField(4);
		this.digitTextField.setPreferredSize(new Dimension(70, 20));

		DefaultListModel<String> l1 = new DefaultListModel<>();
		l1.addElement(this.labelList[0]);
		l1.addElement(this.labelList[1]);
		l1.addElement(this.labelList[2]);
		l1.addElement(this.labelList[3]);
		this.list = new JList<>(l1);
		this.list.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 5));
		this.list.setPreferredSize(new Dimension(70, 75));
		this.list.setBackground(null);
		this.list.setBorder(blackLine);


		createInP.add(list);
		createInP.add(colLabel);
		createInP.add(colSpinner);
		createInP.add(rowLabel);
		createInP.add(rowSpinner);
		createInP.add(digitLabel);
		createInP.add(digitTextField);
		return createInP;
	}

	/**
	 * Metoda tworzaca panel z tabela
	 * @return zwraca obiekt JPanel
	 */
	public JPanel createTablePanel() {
		JPanel createTP = new JPanel();
		blackLine = BorderFactory.createLineBorder(Color.gray);
		titledBorder = BorderFactory.createTitledBorder(blackLine, "Tabela");
		titledBorder.setTitleJustification(TitledBorder.CENTER);
		createTP.setBorder(titledBorder);
		createTP.setLayout(new BorderLayout());

		this.calculation = new Calc();
		this.table = new JTable(this.calculation);

		this.table.setRowHeight(25);
		this.table.setPreferredSize(new Dimension(600, 125));
		createTP.add(new JScrollPane(table));
		createTP.setVisible(true);
		return createTP;
	}

	/**
	 * Metoda tworzaca panel z wynikami
	 * @return zwraca obiekt JPanel
	 */
	public JPanel createResultPanel(){
		JPanel createRP = new JPanel();
		blackLine = BorderFactory.createLineBorder(Color.gray);
		titledBorder = BorderFactory.createTitledBorder(blackLine, "Wynik");
		titledBorder.setTitleJustification(TitledBorder.CENTER);
		createRP.setBorder(titledBorder);
		createRP.setLayout(new BorderLayout());
		createRP.setPreferredSize(new Dimension(600,65));

		resultTextArea = new JTextArea();
		this.resultTextArea.setLineWrap(true);
		// this.resulTextArea.setEditable(false); // edycja pola TextArea
		this.resultTextArea.append(" -->\n");
		createRP.add(new JScrollPane(resultTextArea),BorderLayout.CENTER);
		return createRP;
	}


	/**
	 * Metoda obslugujaca zdarzenie akcji
	 */
	@Override
	public void actionPerformed(ActionEvent ae) {
		if (ae.getSource() == this.addButton) {
			this.addNumberToTable();
		} else if (ae.getSource() == this.clearButton) {
			this.resetTable();
		} else if (ae.getSource() == this.saveButton) {
			MyLogger.writeLog("INFO","Proba zapisania do pliku");
			this.saveToFile();
		} else if (ae.getSource() == this.executeButton) {
			int index = this.list.getSelectedIndex();
			this.calculateTable(index);
		}
	}
	/**
	 * Metoda dodajaca liczby do tabeli
	 */
	public void addNumberToTable() {
		String str = this.digitTextField.getText();
		if (this.validation(str)) {
			Integer number = Integer.parseInt(str);
			int row = this.rowValue.getNumber().intValue() - 1;
			int col = this.colValue.getNumber().intValue() - 1;
			this.calculation.setValue(number, row, col); //wpisz do tablicy
			this.resultTextArea.append("Dodanie: " + number + " do " + "wiersza " + (row + 1) + " i kolumnie " + (col + 1) + "\n");
			InfoBottomPanel.setInfoString("Dodano liczbe do tablicy");
		} else {
			JOptionPane.showMessageDialog(this, "Sprobuj jeszcze raz!");
		}
	}


	/**
	 * Metoda sprawdzajaca wprowadzane dane do obliczen
	 * @param obj zmienna określająca liczbe
	 * @return zwraca wartosc boolowska
	 */
	public boolean validation(String obj) {
		int liczba = obj.length();
		if (liczba <= 0) {
			return false;
		} else if(liczba > 6) {
			return false;
		}
		try{
			double letter = Double.parseDouble(obj);
		}catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(this, "Podano litere!");
			return false;
		}
		return true;
	}
	/**
	 * Metoda czyszczaca tabele
	 */
	public void resetTable() {
		this.calculation.clearTable();
		this.resultTextArea.append("Wyzerowanie tablicy\n");
		InfoBottomPanel.setInfoString("Wyzerowano tablice");
	}
	/**
	 * Metoda obslugujaca operacje na tabeli
	 * @param index ustalony indeks przypisany do kazdej operacji
	 */
	public void calculateTable(int index) {
		int intIn;
		if (index == 0) {
			intIn = this.calculation.calcSum();
			this.resultTextArea.append("Suma tablicy wynosi: " + intIn + "\n");
			InfoBottomPanel.setInfoString("Obliczono sume");
		} else if (index == 1) {
			double avg = this.calculation.calcAvg();
			this.resultTextArea.append("Srednia tablicy 25 elementow wynosi: " + avg + "\n");
			InfoBottomPanel.setInfoString("Obliczono srednia");
		} else if (index == 3) {
			intIn = this.calculation.calcMin();
			this.resultTextArea.append("MIN tablicy wynosi: " + intIn + "\n");
			InfoBottomPanel.setInfoString("Obliczono wartosc minimalna ");
		} else if (index == 2) {
			intIn = this.calculation.calcMax();
			this.resultTextArea.append("MAX tablicy wynosi: " + intIn + "\n");
			InfoBottomPanel.setInfoString("Obliczono wartosc maksymalna");
		} else {
			JOptionPane.showMessageDialog(this, "Nie wybrano zadnej opcji arytmetycznej!");
		}

	}

	/**
	 * Metoda zapisujaca do pliku
	 */
	public void saveToFile() {
		String str = this.calculation.getStringValuesTable() + "\n";
		if (!this.openFile(this.fileName)) {
			JOptionPane.showMessageDialog(this, "Blad otwarcia pliku ");
			MyLogger.writeLog("ERROR","Blad otwarcia pliku");
			System.out.println("ERROR - Blad otwarcia pliku");
		} else {
			try {
				this.file.write(str.getBytes());
				JOptionPane.showMessageDialog(this, "Zapisano pomyslnie! ");
				MyLogger.writeLog("INFO","Zapisano pomyslnie do pliku.");
			} catch (IOException e) {
				JOptionPane.showMessageDialog(this, "Wystapil nieznany blad podczas zapisu");
				MyLogger.writeLog("ERROR","Wystapil nieznany blad podczas zapisu");
				System.out.println("ERROR - Wystapil nieznany blad podczas zapisu");
			} finally {
				this.closeFile();
			}
		}
	}
	/**
	 * Metoda sprawdzajaca sposobnosc otwarcia pliku
	 * @param filename nazwa pliku
	 */
	private boolean openFile(String filename) {
		try {
			this.file = new FileOutputStream(filename);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	/**
	 * Metoda zamykajaca plik
	 */
	private void closeFile() {
		try {
			if (this.file != null) {
				this.file.close();
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this, "Blad zamkniecia pliku ");
			MyLogger.writeLog("ERROR","Blad zamkniecia pliku");
			System.out.println("ERROR - Blad zamkniecia pliku");
		}

	}


	/**
	 * Metoda okreslajaca wartosci odstepow od krawedzi panelu
	 */
	public Insets getInsets() {
		return new Insets(5,10,10,10);
	}
}