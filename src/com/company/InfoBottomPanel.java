package com.company;

import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Color;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
/**
 * Program <code>Zadanie projektowe</code>
 * Klasa <code>InfoBottomPanel</code> definiujaca dolna belke statusu
 * @author Michal Staruch
 * @version 1.0	10/04/2020
 */
public class InfoBottomPanel extends JPanel {

	private static JTextField infoTF;
	private JLabel infoLabel;
	/**
	 * Konstruktor bezparametrowy klasy InfoBottomPanel
	 */
	public InfoBottomPanel() {
		try {
			createGUI();
		}
		catch(Exception e) {
			MyLogger.writeLog("ERROR","Blad podczas tworzenia InfoBottomPanel");
			System.out.println("ERROR - Blad podczas tworzenia InfoBottomPanel");
		}
	}
	/**
	 * Metoda tworzaca graficzny interfejs uzytkownika
	 */
	public void createGUI() {
	 	this.setBackground(new Color(210,210,210));
		this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		
		// Utworzenie etykiet tekstowych
		infoLabel = new JLabel("Status:");
		
		// Utworzenie obiektow TextField
		infoTF = new JTextField("OK!");
		infoTF.setMinimumSize(new Dimension(200,20));
	 	infoTF.putClientProperty("JComponent.sizeVariant", "small");
		this.add(infoLabel);
		this.add(Box.createHorizontalStrut(10));
		this.add(infoTF);
		this.add(Box.createHorizontalStrut(20));
	}
	/**
	 * Publiczna metoda ustawiajaca zmienna infoString
	 * @param infoString nowa wartosc zmiennej infoString 
	 */
	public static void setInfoString(String infoString) {
		infoTF.setText(infoString);
	}
	/**
	 * Metoda okreslajaca wartosci odstepow od krawedzi panelu 
	 * (top,left,bottom,right)
	 */
	public Insets getInsets() {
		return new Insets(5,5,3,5);
	}
}
