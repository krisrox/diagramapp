package com.company;

import javax.swing.table.AbstractTableModel;


/**
 * Program <code>Zadanie projektowe</code>
 * Klasa <code>Calc</code> definiujaca funkcje matematyczne aplikacji
 * @author Michal Staruch
 * @version 1.0	10/04/2020
 */

public class Calc extends AbstractTableModel {
	private final int[][] data = new int[5][5];

	public Calc() {
		this.clearTable();
	}

	public int getColumnCount() {
		return 5;
	}

	public int getRowCount() {
		return 5;
	}

	public Object getValueAt(int row, int col) {
		return this.data[row][col];
	}

	public String getStringValuesTable() {
		StringBuilder str = new StringBuilder();
		for(int i = 0; i < 5; ++i) {
			for(int j = 0; j < 5; ++j) {
				str.append(this.data[i][j]).append(" ");
			}
		}
		return str.toString();
	}


	public void setValue(Integer value, int row, int col) {
		this.data[row][col] = value;
		this.fireTableDataChanged();
	}


	public void clearTable() {
		for(int i = 0; i < 5; ++i) {
			for(int j = 0; j < 5; ++j) {
				this.data[i][j] = 0;
			}
		}
		this.fireTableDataChanged(); //uaktualnij w locie
	}


	public int calcSum() {
		int sum = 0;
		for(int i = 0; i < 5; ++i) {
			for(int j = 0; j < 5; ++j) {
				sum = sum + this.data[i][j];
			}
		}

		return sum;
	}

	public Double calcAvg() {
		double avg = 0.0;
		int sum = this.calcSum();
		if (sum > 0) {
			avg = (double) sum / 25.0;
		}
		return avg;
	}

	public int calcMin() {
		int min = this.data[0][0];
		for(int i = 0; i < 5; ++i) {
			for(int j = 0; j < 5; ++j) {
				if (this.data[i][j] < min) {
					min = this.data[i][j];
				}
			}
		}
		return min;
	}

	public int calcMax() {
		int max = this.data[0][0];
		for(int i = 0; i < 5; ++i) {
			for(int j = 0; j < 5; ++j) {
				if (this.data[i][j] > max) {
					max = this.data[i][j];
				}
			}
		}
		return max;
	}
}
