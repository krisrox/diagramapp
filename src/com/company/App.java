package com.company;

import java.awt.*;
import java.awt.event.*;
import java.awt.print.PageFormat;
import java.awt.print.PrinterJob;
import javax.swing.*;
import com.l2fprod.common.swing.JButtonBar;



/**
 * Program <code>Zadanie projektowe</code>
 * Klasa <code>App</code> definiujaca glowne okno aplikacji
 * @author Michal Staruch
 * @version 1.0	23/04/2020
 */
public class App extends JFrame implements ActionListener, Runnable {
    private static final int WIDTH = 650;
    private static final int HEIGHT = 670;
    private final JPanel contentPane;
    HelpWindow helpWindow = null;
    AboutWindow aboutWindow = null;
    TOTD tipsWindow = null;
    InfoBottomPanel infoPanel = null;
    CenterPanel centerPanel = null;
    DiagramWindow diagramWindow = null;
    MainWindow welcomeWindow = null;
    String[] optionJOptionPane = { "Tak", "Nie" };
    MyLogger logger = null;

    /**
     * Zmienne tworzace menu
     */
    JMenuBar menuBar;
    JMenu fileMenu, editMenu, viewMenu, helpMenu; //widoczne menu
    JMenuItem filePrintMenuItem, fileExitMenuItem, //rozwijana lista po kliknieciu
            helpContextMenutem, helpAboutMenuItem;
    JCheckBoxMenuItem viewStatusBarMenuItem, viewJToolBarMenuItem; //checkboxy
    /**
     * Zmienne tworzace pasek narzedziowy
     */
    JToolBar jToolBar;
    JButton jtbPrint, jtbExit, jtbHelp, jtbAbout;
    JButton cjbbMain, cjbbDiag;
    /**
     * Zmienne tworzace pasek nawigacyjny z biblioteki zewnetrzenj
     */
    JButtonBar jbtBar;
    /**
     *  definicja etykiet tekstowych
     */
    String[] labelMenu = {"Plik", "Edycja", "Widok", "Pomoc"};
    String[] labelFileMenuItem = {"Drukuj", "Zamknij"};
    String[] labelViewMenuItem = {"Ukryj pasek statusu","Ukryj pasek narzedziowy"};
    String[] labelHelpMenuItem = {"Kontekst pomocy", "Informacje o programie"};


    /**
     *  definicja etykiet opisujacych w tzw. dymkach
     */
    String[] tooltipFileMenu = {"Drukowanie", "Zamkniecie aplikacji"};
    String[] tooltipHelpMenu = {"Uruchomienie pomocy", "Informacje o programie"};
    String[] tooltipNav = {"Strona glowna", "Wykres"};

    Icon iconPrint, iconExit, iconHelpContext, iconAbout, iconMain, iconDiag;
    Icon mIconPrint, mIconExit, mIconHelpContext, mIconAbout;


    /**
     * Konstruktor bezparametrowy klasy
     */
    public App() {
        this.setTitle("Zadanie Projektowe - Michal Staruch");
        //this.setLocationRelativeTo(null); //alternatywnie ustawia lokalizacje zawsze na srodku ekranu

        // definicja zdarzenia zamkniecia okna w klasie anonimowej
        this.addWindowListener	(new WindowAdapter(){
            public void windowClosing(WindowEvent e){
                closeWindow();
                MyLogger.writeLog("INFO", "Zamkniecie aplikacji");
                dispose();
                System.exit(0);
            }
        });

        // Rozmieszczenie okna na srodku ekranu
        Dimension frameSize = new Dimension(WIDTH,HEIGHT);
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();//pobranie rozdzielczosci pulpitu
        if(frameSize.height > screenSize.height) frameSize.height = screenSize.height;
        if(frameSize.width > screenSize.width) frameSize.width = screenSize.width;
        setSize(frameSize);
        setLocation((screenSize.width-frameSize.width)/2,
                (screenSize.height-frameSize.height)/2);

        // Utworzenie glownego kontekstu (ContentPane)
        contentPane = (JPanel) this.getContentPane();
        contentPane.setLayout(new BorderLayout());
        this.logger = new MyLogger();

        // utworzenie GUI w watku zdarzeniowym
        try {
            javax.swing.SwingUtilities.invokeAndWait(new Runnable() {
                public void run() {
                    createIcons();
                    createMenu();
                    createGUI();
                }
            });
        }
        catch(Exception e) {
            MyLogger.writeLog("ERROR","Blad podczas tworzenia GUI aplikacji");
            System.out.println("ERROR - Blad podczas tworzenia GUI aplikacji");
        }

        // utworzenie watku uruchamiajacgo okno logowania ???????????????????????????????????????????????????????
        Thread thread = new Thread(this);
        thread.start();
    }
    /**
     * Metoda wytku uruchamiajaca z oponieniem 1000 ms okno Tips of the day po uruchomieniu aplikacji
     */
    public void run() {
        try {
            Thread.sleep(1000); // opoznienie 1 sekunda
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        // uruchomienie okna logowania
        openTOTD();
    }
    /**
     * Metoda tworząca ikony wykorzystywane w programie
     */
    private void createIcons() {
        try {
            // Utworzenie ikon 24 * 24 px dla paska toolbar
            iconPrint = createMyIcon("print.jpg");
            iconExit = createMyIcon("close.jpg");
            iconHelpContext = createMyIcon("help_context.jpg");
            iconAbout = createMyIcon("about.jpg");

            // Utworzenie ikon 32 * 23 px dla paska nawigacyjnego
            iconMain = createMyIcon("welcome32x32.png");
            iconDiag = createMyIcon("folder32x32.png");

            // Utworzenie ikon 16 * 16 px dla MenuItem
            mIconPrint = createMyIcon("min_print.jpg");
            mIconExit = createMyIcon("min_close.jpg");
            mIconHelpContext = createMyIcon("min_help_context.jpg");
            mIconAbout = createMyIcon("min_about.jpg");


        }
        catch(Exception e) {
            MyLogger.writeLog("ERROR", "Blad tworzenia ikon");
            System.out.println("ERROR - Blad tworzenia ikon");
        }
    }

    /**
     * Metoda tworząca menu okna głównego
     */
    private void createMenu() {
        try{
            // Utworzenie paska menu
            menuBar = new JMenuBar();
            // Utworzenie pol menu glownego
            fileMenu = createJMenu(labelMenu[0], KeyEvent.VK_P);
            editMenu = createJMenu(labelMenu[1], KeyEvent.VK_E);
            viewMenu = createJMenu(labelMenu[2], KeyEvent.VK_W);
            helpMenu = createJMenu(labelMenu[3], KeyEvent.VK_O);

            // Utworzenie pol podmenu
            filePrintMenuItem = createJMenuItem(labelFileMenuItem[0],mIconPrint,
                    KeyStroke.getKeyStroke(KeyEvent.VK_D,ActionEvent.ALT_MASK));
            fileExitMenuItem = createJMenuItem(labelFileMenuItem[1],mIconExit,
                    KeyStroke.getKeyStroke(KeyEvent.VK_X,ActionEvent.ALT_MASK));

            // utworzenie MenuItem dla viewMenu
            viewStatusBarMenuItem = createJCheckBoxMenuItem(labelViewMenuItem[0],false);
            viewJToolBarMenuItem = createJCheckBoxMenuItem(labelViewMenuItem[1],false);

            // utworzenie obiektow MenuItem dla helpMenu
            helpContextMenutem = createJMenuItem(labelHelpMenuItem[0],
                    mIconHelpContext,KeyStroke.getKeyStroke(KeyEvent.VK_H, ActionEvent.ALT_MASK));
            helpAboutMenuItem = createJMenuItem(labelHelpMenuItem[1],mIconAbout,
                    KeyStroke.getKeyStroke(KeyEvent.VK_I,ActionEvent.ALT_MASK));

            // dodanie utworzonych elementow menu dopaska menu JMenuBar
            fileMenu.add(filePrintMenuItem);
            fileMenu.addSeparator();
            fileMenu.add(fileExitMenuItem);
            viewMenu.add(viewStatusBarMenuItem);
            viewMenu.add(viewJToolBarMenuItem);
            helpMenu.add(helpContextMenutem);
            helpMenu.add(helpAboutMenuItem);
            menuBar.add(fileMenu);
            menuBar.add(editMenu);
            menuBar.add(viewMenu);
            menuBar.add(helpMenu);
            this.setJMenuBar(menuBar);
        }catch (Exception e){
            MyLogger.writeLog("ERROR", "Blad tworzenia menu");
            System.out.println("ERROR - Blad tworzenia menu");
        }
    }
    /**
     * Prywatna metoda tworzaca GUI
     */
    private void createGUI() {
        // Utworzenie paska narzedziowego
        jToolBar = new JToolBar();
        createJToolBar(jToolBar);

        jbtBar = new JButtonBar(JButtonBar.VERTICAL);
        createJButtonBar(jbtBar);

        // Utworzenie panelu informacyjnego umieszczonego na dole okna
        infoPanel = new InfoBottomPanel();

        // Utworzenie panelu centralnego
        centerPanel = new CenterPanel();

        contentPane.add(jToolBar, BorderLayout.NORTH);
        contentPane.add(infoPanel, BorderLayout.SOUTH);
        contentPane.add(centerPanel, BorderLayout.CENTER);
    }
    /**
     * Prywatna metoda tworząca pasek narzędziowy
     * @param cjtb zmienna typu JToolBar
     */
    private void createJToolBar(JToolBar cjtb) {
        cjtb.setFloatable(false);
        cjtb.add(Box.createHorizontalStrut(5));

        // Utworzenie przyciskow paska narzedziowego
        jtbPrint = createJButtonToolBar(tooltipFileMenu[0],iconPrint,true);
        jtbExit = createJButtonToolBar(tooltipFileMenu[1],iconExit,true);
        jtbHelp = createJButtonToolBar(tooltipHelpMenu[0],iconHelpContext,true);
        jtbAbout = createJButtonToolBar(tooltipHelpMenu[1],iconAbout,true);

        // dodanie przyciskow do paska narzedziowego
        cjtb.add(jtbPrint);
        cjtb.add(jtbExit);
        cjtb.addSeparator();
        cjtb.add(jtbHelp);
        cjtb.add(jtbAbout);
    }

    /**
     * Prywatna metoda tworząca pasek nawigacyjny z dodatkowej biblioteki
     * @param cjbb zmienna typu JButtonBar
     */
    private void createJButtonBar(JButtonBar cjbb){
        this.add("West", cjbb);

        // Utworzenie przyciskow paska nawigacyjnego
        cjbbMain = createJButtonBarNav(tooltipNav[0], iconMain,true);
        cjbbDiag = createJButtonBarNav(tooltipNav[1], iconDiag,true);

        // dodanie przyciskow do paska nawigacyjnego
        cjbb.add(cjbbMain);
        cjbb.add(cjbbDiag);
    }
    /**
     * Prywatna metoda tworząca obiekt typu JButton dla paska nawigacyjnego
     * @param tooltip zmienna określająca podpowiedz dla przycisku
     * @param icon zmienna określająca obrazek przypisany do przycisku
     * @param enable zmienna logiczna określająca czy przycisk jest aktywny
     * @return zwraca utworzony obiekt typu JButton
     */
    private JButton createJButtonBarNav(String tooltip,Icon icon, boolean enable) {
        JButton jbb = new JButton("",icon);
        jbb.setToolTipText(tooltip);
        jbb.addActionListener(this);
        jbb.setEnabled(enable);
        return jbb;
    }

    /**
     * Prywatna metoda tworząca obiekt typu Icon
     * @param file zmienna określająca nazwę pliku
     * @return zwraca obiekt typu Icon
     */
    public Icon createMyIcon(String file) {
        String name = "/pic/"+file;
        return new ImageIcon(getClass().getResource(name));
    }
    /**
     * Prywatna metoda tworząca obiekt typu JMenu
     * @param name zmienna określająca nazwę
     * @param keyEvent zmienna określająca klawisz skrótu
     * @return zwraca utworzony obiekt typu JMenu
     */
    private JMenu createJMenu(String name, int keyEvent) {
        JMenu jMenu = new JMenu(name);
        jMenu.setMnemonic(keyEvent);
        jMenu.setEnabled(true);
        return jMenu;
    }
    /**
     * Prywatna metoda tworząca obiekt typu JMenuItem
     * @param name zmienna określająca nazwę
     * @param icon zmienna określająca iconę wyświetlaną wraz z nazwę
     * @param key zmienna określajaca klawisze akceleracji dostępu
     * @return zwraca utworzony obiekt typu JMenuItem
     */
    private JMenuItem createJMenuItem(String name, Icon icon, KeyStroke key) {
        JMenuItem jMI;
        if(icon != null)
            jMI = new JMenuItem(name,icon);
        else jMI = new JMenuItem(name);
        jMI.setAccelerator(key);
        jMI.addActionListener(this);
        jMI.setEnabled(true);
        return jMI;
    }
    /**
     * Prywatna metoda tworząca obiekt typu JCheckBoxMenuItem
     * @param name zmienna określająca nazwę
     * @param enable zmienna logiczna określająca czy podmenu jest aktywne
     * @return utworzony obiekt typu JCheckBoxMenuItem
     */
    private JCheckBoxMenuItem createJCheckBoxMenuItem(String name, boolean enable) {
        JCheckBoxMenuItem jcbmi = new JCheckBoxMenuItem(name, enable);
        jcbmi.addActionListener(this);
        jcbmi.setState(enable);  // ustawienie wartości początkowej (nie zaznaczony)
        return jcbmi;
    }
    /**
     * Prywatna metoda tworząca obiekt typu JButton dla paska narzędziowego
     * @param tooltip zmienna określająca podpowiedz dla przycisku
     * @param icon zmienna określająca obrazek przypisany do przycisku
     * @param enable zmienna logiczna określająca czy przycisk jest aktywny
     * @return zwraca utworzony obiekt typu JButton
     */
    private JButton createJButtonToolBar(String tooltip,Icon icon, boolean enable) {
        JButton jb = new JButton("",icon);
        jb.setToolTipText(tooltip);
        jb.addActionListener(this);
        jb.setEnabled(enable);
        return jb;
    }
    /**
     * Prywatna metoda zamykajaca aplikację
     */
    private void closeWindow() {
        // utworzenie okna z potwierdzeniem zamkniecia
        int value = JOptionPane.showOptionDialog(this,
                "Czy chcesz zamknac program? Wybranie przycisku Tak spowoduje " +
                        "jego zamkniecie.", // komunikat
                "Pytanie",// naglowek okna
                JOptionPane.YES_NO_OPTION,  // typ opcji
                JOptionPane.QUESTION_MESSAGE, // typ komunikatu
                null,						// domyslna ikona
                optionJOptionPane, 	 		// zestaw etykiet przycisków
                optionJOptionPane[0]);  	// inicjacja aktywnego przycisku

        if(value == JOptionPane.YES_OPTION) {
            // Zamkniecie aplikacji
            dispose();
            System.exit(0);
        }
    }
    /**
     * Publiczna metoda obslugujaca wydruk
     */
    public void printListForm() {
        try {
            //	setPrintArrayList();
            // Glowny obiekt odpowiedzialny za drukowanie
            PrinterJob job = PrinterJob.getPrinterJob();
            // Obiekt odpowiedzialny za rozmiar i orientacje drukowanej strony
            PageFormat pf = new PageFormat();
            // Wywolanie okna ustawien drukowania
            job.pageDialog(pf);
            // job.setPrintable(ListFrame, pf);
            if(job.printDialog()) {
                job.print();	// Jesli uzytkownik wybral ok drukujemy panel
                InfoBottomPanel.setInfoString("Wydrukowanie listy");
            }
        }
        catch(Exception exc) {
            MyLogger.writeLog("ERROR","Blad drukowania");
            System.out.println("Blad drukowania...");
        }
    }

    /**
     * Metoda generujaca okno tips of the day
     */
    public void openTOTD() {
        tipsWindow = new TOTD(this);
        tipsWindow.setVisible(true);
    }


    /**
     * Metoda obsługująca zdarzenie akcji
     */
    @Override
    public void actionPerformed(ActionEvent ae) {
        if((ae.getSource() == filePrintMenuItem) ||
                (ae.getSource() == jtbPrint)) {
            MyLogger.writeLog("INFO","Otwarcie okna wydruku");
            printListForm();
        }
        else if((ae.getSource() == fileExitMenuItem) ||
                (ae.getSource() == jtbExit)) {
            // Zamkniecie aplikacji
            MyLogger.writeLog("INFO","Zamkniecie aplikacji");
            dispose();
            System.exit(0);
        }
        else if(ae.getSource() == viewStatusBarMenuItem) {
            boolean stan = viewStatusBarMenuItem.getState();
            infoPanel.setVisible(!stan);
        }
        else if(ae.getSource() == viewJToolBarMenuItem) {
            boolean stan = viewJToolBarMenuItem.getState();
            if(stan) jToolBar.setVisible(false);
            else jToolBar.setVisible(true);
        }
        else if((ae.getSource() == helpContextMenutem) ||
                (ae.getSource() == jtbHelp)) {
            if(helpWindow != null) helpWindow.setVisible(true);
            else {
                helpWindow = new HelpWindow();
                helpWindow.setVisible(true);
            }
        }
        else if((ae.getSource() == helpAboutMenuItem) ||
                (ae.getSource() == jtbAbout)) {
            if(aboutWindow != null) aboutWindow.setVisible(true);
            else {
                aboutWindow = new AboutWindow();
                aboutWindow.setVisible(true);
            }
        }
        else if(ae.getSource() == cjbbDiag){
            if(diagramWindow != null) diagramWindow.setVisible(true);
            else {
                diagramWindow = new DiagramWindow();
                diagramWindow.setVisible(true);
            }
        }
        else if(ae.getSource() == cjbbMain){
            if(welcomeWindow != null) welcomeWindow.setVisible(true);
            else {
                welcomeWindow = new MainWindow();
                welcomeWindow.setVisible(true);
            }
        }
    }
    /**
     * Publiczna metoda uruchomieniowa tworząca obiekt klasy
     */
    public static void main(String[] args) {
        //UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        MyLogger.writeLog("INFO","Start Aplikacji");
        System.out.println("Start Aplikacji");
        App finalWindow = new App();
        finalWindow.setVisible(true);
    }
}