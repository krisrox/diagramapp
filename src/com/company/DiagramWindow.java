package com.company;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.AxisLocation;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;

import javax.swing.*;
import java.awt.*;


/**
 * Program <code>Zadanie projektowe</code>
 * Klasa <code>DiagramWindow</code> definiujaca okno wyswietalnia wykresu
 * @author Michal Staruch
 * @version 1.0	10/04/2020
 */

public class DiagramWindow extends JDialog {

	CategoryDataset dataset = createDataset();
	JFreeChart chart = createChart(dataset);
	Calc calucation = null;

	/**
	 * Konstruktor bezparametrowy klasy <code>DiagramWindow</code>
	 */
	public DiagramWindow() {
		this.setTitle("Diagram horyzontalny");
		this.setModal(false);
		this.setResizable(true);
		this.setSize(500,270);
		this.setLocationRelativeTo(null); //alternatywnie ustawia lokalizacje zawsze na srodku ekranu

		try {
			createGUI();
		}
		catch(Exception e) {
			MyLogger.writeLog("ERROR","Blad podczas tworzenia DiagramWindow");
			System.out.println("ERROR - Blad podczas tworzenia DiagramWindow");
		}
	}
	/**
	 * Metoda tworzaca rozmieszczajaca okno wykresu
	 *
	 */
	public void createGUI() {
		this.setLayout(new GridLayout(1,2,5,5));

		ChartPanel chartPanel = new ChartPanel(chart);
		chartPanel.setPreferredSize(new java.awt.Dimension(500, 270));
		this.setContentPane(chartPanel);
	}


	/**
	 * Tworzy zestaw danych
	 *
	 * @return zestaw danych
	 */
	private CategoryDataset createDataset() {
		// create the dataset...
		final DefaultCategoryDataset dataset = new DefaultCategoryDataset();
/*
		for(int i = 0; i < 5; ++i) {
			for(int j = 0; j < 5; ++j) {
				// row keys... poziom
				String row = String.valueOf(i);
				// column keys... pion
				String col = String.valueOf(j);

				assert this.calucation !=null;
				dataset.addValue((Double)this.calucation.getValueAt(i,j), row,col);

			}
		}

		//dataset.addValue(1.0, series1, category1);
*/

		return dataset;
	}



	/**
	 * Tworzy wykres
	 *
	 * @param dataset  the dataset.
	 *
	 * @return wykres
	 */
	private JFreeChart createChart(final CategoryDataset dataset) {

		final JFreeChart chart = ChartFactory.createBarChart(
				"Wykres",         // chart title
				"Category",                 // domain axis label
				"Value",                // range axis label
				dataset,                    // data
				PlotOrientation.HORIZONTAL, // orientation
				true,                       // include legend
				true,
				false
		);

		// get a reference to the plot for further customisation...
		final CategoryPlot plot = chart.getCategoryPlot();
		plot.setRangeAxisLocation(AxisLocation.BOTTOM_OR_LEFT);

		// change the auto tick unit selection to integer units only...
		final NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
		rangeAxis.setRange(0.0, 100.0);
		rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());

		return chart;
	}
}
